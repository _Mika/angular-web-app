import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, convertToParamMap, ParamMap, Params, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/pluck';

import {SidebarPageAvailable} from '../../components/sidebar/sidebar.component';
import {Store} from "@ngrx/store";
import * as fromView from '../../reducers/allview';
import * as fromMonitoredObjects from '../../reducers/monitoredObjects';
import * as fromRoot from '../../reducers';

// import * as action from '../../actions/allview';
import * as MOaction from '../../actions/monitoredObjects';
// import * as allview from '../../actions/allview';
// import {Subscription} from "rxjs/Subscription";
// import {Http, RequestOptions, Headers} from '@angular/http';
import {MonitoredObjectService} from "../../services/monitored.object.service";
import {MonitoredObject} from "../../models/monitoredObject.model";



@Component({
  selector: 'app-allview',
  templateUrl: './allview.component.html',
  styleUrls: ['./allview.component.css']
})
export class AllviewComponent implements OnInit, OnDestroy {

  // sidebar
  isSidebarOpen = false;
  sidebarWightOpen = '200px';
  sidebarWightClose = '0px';
  // search bar
  buttonColorOpen = '#00bcd4';
  buttonColorClose = '#e6e7e8';
  widthInputOpen = '230px';
  widthInputClose = '0';


  // actionsSubscription: Subscription;
  allviewSate$: Observable<fromView.State>;
  MonitoredObjectsSate$: Observable<fromMonitoredObjects.State>;
  MonitoredObjects$: Observable<MonitoredObject[]>;

  constructor(private router: Router, private store: Store<fromRoot.State>, private mOService: MonitoredObjectService) {

    this.allviewSate$ = this.store.select(fromRoot.getAllSate2);
    this.MonitoredObjectsSate$ = this.store.select(fromRoot.getMonitoredObjects);

    this.MonitoredObjects$ = this.store.select(fromRoot.getFilteredMonitoredObject);

  }

  ngOnInit() {
    this.mOService.startSocketListenner();
  }
  ngOnDestroy() {
    this.mOService.endSocketListenner();
  }

  searchToggle() {

    this.allviewSate$.subscribe(state => {
      const newPageState = Object.assign({}, state);
      if (state.isSearchOpen) {
        newPageState.searchOnMonitoredObjects = '';
      }
        newPageState.isSearchOpen = !newPageState.isSearchOpen;

      this.router.navigate(['/allview', this.createSateParams(newPageState)]);
    });
  }

  detailMode() {
    this.allviewSate$.subscribe(state => {
      const newPageState = Object.assign({}, state);
      newPageState.detailMode = true;
      this.router.navigate(['/allview', this.createSateParams(newPageState)]);
    });
  }

  normalMode() {
    this.allviewSate$.subscribe(state => {
      const newPageState = Object.assign({}, state);
      newPageState.detailMode = false;
      this.router.navigate(['/allview', this.createSateParams(newPageState)]);
    });
  }

  toggleSidebar() {
    this.allviewSate$.subscribe(state => {
      const newPageState = Object.assign({}, state);
      newPageState.isSidebarOpen = !newPageState.isSidebarOpen;
      this.router.navigate(['/allview', this.createSateParams(newPageState)]);
    });
  }

  sidebarPageSelection(page: SidebarPageAvailable) {
    this.allviewSate$.subscribe(state => {
      const newPageState = Object.assign({}, state);
        newPageState.sidebarActivePage = page;
      this.router.navigate(['/allview', this.createSateParams(newPageState)]);
    });
  }
  sidebarSortSelection(sort: string) {
    this.allviewSate$.subscribe(state => {
      const newPageState = Object.assign({}, state);
      newPageState.currentSort = sort;
      this.router.navigate(['/allview', this.createSateParams(newPageState)]);
    });
  }


  handleSearch(value) {
    this.allviewSate$.subscribe(state => {
      const newPageState = Object.assign({}, state);
      newPageState.searchOnMonitoredObjects = value;
      this.router.navigate(['/allview', this.createSateParams(newPageState)]);
    });
  }


  private createSateParams(sate: fromView.State): Params {
    const r: any = {};
    for (let key in sate) {
      r[key] = sate[key];
    }
    return r;
  }
}


