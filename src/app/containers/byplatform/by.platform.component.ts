import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, convertToParamMap, ParamMap, Params, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/pluck';

import {SidebarPageAvailable} from '../../components/sidebar/sidebar.component';
import {Store} from "@ngrx/store";
import * as fromByPlatform from '../../reducers/byPlatform';
import * as fromMonitoredObjects from '../../reducers/monitoredObjects';
import * as fromRoot from '../../reducers';

// import * as action from '../../actions/allview';
// import * as MOaction from '../../actions/monitoredObjects';
// import * as allview from '../../actions/allview';
// import {Subscription} from "rxjs/Subscription";
// import {Http, RequestOptions, Headers} from '@angular/http';
import {MonitoredObjectService} from '../../services/monitored.object.service';



@Component({
  selector: 'app-by-platform',
  templateUrl: './by.platform.component.html',
  styleUrls: ['./by.platform.component.css']
})
export class ByPlatformComponent implements OnInit, OnDestroy {

  // sidebar
  isSidebarOpen = false;
  sidebarWightOpen = '200px';
  sidebarWightClose = '0px';
  // search bar
  buttonColorOpen = '#00bcd4';
  buttonColorClose = '#e6e7e8';
  widthInputOpen = '230px';
  widthInputClose = '0';
  // isSearchOpen = false;
  searchValue = '';

  // state: MainPageState = defaultState;

  // actionsSubscription: Subscription;
  byPlatformSate$: Observable<fromByPlatform.State>;
  MonitoredObjectsSate$: Observable<fromMonitoredObjects.State>;

  constructor(private router: Router, private store: Store<fromRoot.State>, private mOService: MonitoredObjectService) {

    this.byPlatformSate$ = this.store.select(fromRoot.getAllSate2);
    this.MonitoredObjectsSate$ = this.store.select(fromRoot.getMonitoredObjects);
  }

  ngOnInit() {
    this.mOService.startSocketListenner();
  }
  ngOnDestroy() {
    this.mOService.endSocketListenner();
  }

  serachToggle() {

    this.byPlatformSate$.subscribe(state => {
      const newPageState = Object.assign({}, state);
      newPageState.isSearchOpen = !newPageState.isSearchOpen;
      this.router.navigate(['/byplatform', this.createSateParams(newPageState)]);
    });
  }

  detailMode() {
    this.byPlatformSate$.subscribe(state => {
      const newPageState = Object.assign({}, state);
      newPageState.detailMode = true;
      this.router.navigate(['/byplatform', this.createSateParams(newPageState)]);
    });
  }

  normalMode() {
    this.byPlatformSate$.subscribe(state => {
      const newPageState = Object.assign({}, state);
      newPageState.detailMode = false;
      this.router.navigate(['/byplatform', this.createSateParams(newPageState)]);
    });
  }

  toggleSidebar() {
    this.byPlatformSate$.subscribe(state => {
      // console.log('test 1', state);
      const newPageState = Object.assign({}, state);
      // console.log('test 2', newPageState);
      newPageState.isSidebarOpen = !newPageState.isSidebarOpen;
      // console.log('test 3', newPageState);
      this.router.navigate(['/byplatform', this.createSateParams(newPageState)]);
    });
  }

  sidebarPageSelection(page: SidebarPageAvailable) {
    this.byPlatformSate$.subscribe(state => {
      const newPageState = Object.assign({}, state);
      // const activePage: SidebarPageAvailable  = <SidebarPageAvailable>SidebarPageAvailable[page];
      // console.log( activePage);
        newPageState.sidebarActivePage = page;
      this.router.navigate(['/byplatform', this.createSateParams(newPageState)]);
    });
  }


  private createSateParams(sate: fromByPlatform.State): Params {
    const r: any = {};
    for (let key in sate) {
      r[key] = sate[key];
    }
    return r;
  }
}


