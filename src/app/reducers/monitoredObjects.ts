import * as monitoredObjectsAction from '../actions/monitoredObjects';
import {MonitoredObjectService} from '../services/monitored.object.service';
import {MonitoredObject} from '../models/monitoredObject.model';

// Statuses
const statuses = ['critical', 'degraded', 'up'];

export interface State {
  loaded: boolean;
  monitoredObjects: MonitoredObject[];
  // Search on MonitoredObject
  searchOnMonitoredObjects: string;
  alphabetical: number; // 1 for alphabetical order & -1 for opposite
  // Sort
  statusesSortBy: string[];
  currentSort: string;

  // counter
  counterUp: number;
  counterDegraded: number;
  counterCritical: number;
}

export const initialState: State = {
  loaded: false,
  monitoredObjects: [],
  searchOnMonitoredObjects: '',
  alphabetical: 1,
  statusesSortBy: statuses,
  currentSort: 'upToCritical',
  // counter
  counterUp: 0,
  counterDegraded: 0,
  counterCritical: 0,
};

export function reducer(state = initialState, action: monitoredObjectsAction.Actions): State {
  switch (action.type) {
    case monitoredObjectsAction.RETRIEVE_MONITORED_OBJECTS:
    {
      const monitoredObjects = action.payload.monitoredObjects;
      const loaded = true;
      return {...updateCounter(state, monitoredObjects), monitoredObjects, loaded};
    }
    case monitoredObjectsAction.UPDATE_MONITORED_OBJECTS:
    {
      const monitoredObjects = state.monitoredObjects;
      const moIndex = monitoredObjects.findIndex(mo => mo.id === action.payload.monitoredObject.id );
      if (moIndex !== -1) {
        // update
        monitoredObjects[moIndex] = action.payload.monitoredObject;
      } else {
        // add
        monitoredObjects.push(action.payload.monitoredObject);
      }
      return {...updateCounter(state, monitoredObjects), monitoredObjects};
    }
    case monitoredObjectsAction.DELETE_MONITORED_OBJECTS:
    {
      const monitoredObjects =  state.monitoredObjects;
      let newMonitoredObjects = [];
      const moIndex = monitoredObjects.findIndex(mo => mo.id === action.payload.monitoredObject.id );
      if (moIndex !== -1) {
        // delete
        newMonitoredObjects = monitoredObjects.splice(moIndex, 1);
      } else {
        // not found object
        console.log('Monitored object not found');
      }
      return {...state, newMonitoredObjects};
    }
    case monitoredObjectsAction.SEARCH_MONITORED_OBJECTS:
    {
      const searchOnMonitoredObjects = action.payload.searchOnMonitoredObjects;
      return {...state, searchOnMonitoredObjects};
    }
    case monitoredObjectsAction.SORT_MONITORED_OBJECTS:
    {
      const currentSort = action.payload.currentSort;
      let statusesSortBy = [];
      if (currentSort === 'upToCritical') {
        statusesSortBy = statuses.slice();
        statusesSortBy.reverse();
      } else if (currentSort === 'criticalToUp') {
        statusesSortBy = statuses.slice();
      } else {
        statusesSortBy = [];
      }
      return {...state, statusesSortBy, currentSort};
    }
    default:
      return state;
  }
}

export const getSate = (state: State) => state;

export const filteredMonitoredObject = (state: State) => state.monitoredObjects
  .filter(monitoredObjects => monitoredObjects.alias.match(new RegExp(state.searchOnMonitoredObjects, 'gi')))
  .sort(function (a, b) {
    let aStatusId;
    let bStatusId;
    if (a.status && a.status.services.length > 0) {
      aStatusId = convertToNumber(a.status.services[0].status, state);
    } else {
      aStatusId = 0;
    }
    if (b.status && b.status.services.length > 0) {
      bStatusId = convertToNumber(b.status.services[0].status, state);
    } else {
      bStatusId = 0;
    }
    if (aStatusId === bStatusId) {
      if (a.alias.toLowerCase() < b.alias.toLowerCase()) return -1 * state.alphabetical;
      if (a.alias.toLowerCase() > b.alias.toLowerCase()) return state.alphabetical;
      return 0;
    }
    return bStatusId - aStatusId;
  });

const convertToNumber = function (status, state: State) {
  switch (status) {
    case state.statusesSortBy[0]:
      return 3;
    case state.statusesSortBy[1]:
      return 2;
    case state.statusesSortBy[2]:
      return 1;
    default:
      return 0;
  }
};


const updateCounter = function (state: State, monitoredObjects: MonitoredObject[]): State {
  state.counterUp = 0;
  state.counterDegraded = 0;
  state.counterCritical = 0;

  monitoredObjects.forEach(element => {
    if (element.status) {
      if (element.status.services.length > 0) {
        if (element.status.services[0].status === 'up') {
          state.counterUp++;
        }
        if (element.status.services[0].status === 'degraded') {
          state.counterDegraded++;
        }
        if (element.status.services[0].status === 'critical') {
          state.counterCritical++;
        }
      }
    }
  });
  return state;
};



