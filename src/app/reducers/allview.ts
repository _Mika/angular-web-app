import * as allview from '../actions/allview';
import {SidebarPageAvailable} from '../components/sidebar/sidebar.component';

export interface State {
  isSidebarOpen: boolean;
  isSearchOpen: boolean;
  sidebarActivePage: SidebarPageAvailable;
  detailMode: boolean;
  searchOnMonitoredObjects: string;
  currentSort: string;
}

export const initialState: State = {
  isSidebarOpen: false,
  isSearchOpen: true,
  sidebarActivePage: SidebarPageAvailable.Filter,
  detailMode: false,
  searchOnMonitoredObjects: '',
  currentSort: 'upToCritical'
};



export function reducer(state = initialState, action: allview.Actions): State {
  switch (action.type) {
    case allview.ALLVIEW_TOGGLE_SIDEBAR:
    {
      const newState = state;
      newState.isSidebarOpen = !newState.isSidebarOpen;
      return newState;
    }
    case allview.ALLVIEW_LOAD_STATE:
      return action.payload.pageState;

    default:
      return state;
  }
}

export const getSate = (state: State) => state;
