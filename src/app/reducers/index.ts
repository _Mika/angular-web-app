import {
  ActionReducerMap,
  createSelector,
  createFeatureSelector,
  ActionReducer,
  MetaReducer,
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { RouterStateUrl } from '../shared/utils';
import * as fromRouter from '@ngrx/router-store';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */

import * as fromLayout from '../core/reducers/layout';
import * as fromAllview from './allview';
import * as fromByPlatform from './byPlatform';
import * as fromMonitoredObjects from './monitoredObjects';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface State {
  layout: fromLayout.State;
  allview: fromAllview.State;
  byPlatform: fromByPlatform.State;
  monitoredObjects: fromMonitoredObjects.State;
  routerReducer: fromRouter.RouterReducerState<RouterStateUrl>;
}

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const reducers: ActionReducerMap<State> = {
  layout: fromLayout.reducer,
  allview: fromAllview.reducer,
  byPlatform: fromByPlatform.reducer,
  monitoredObjects: fromMonitoredObjects.reducer,
  routerReducer: fromRouter.routerReducer,
};

// console.log all actions
export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return function(state: State, action: any): State {
    console.log('state', state);
    console.log('action', action);

    return reducer(state, action);
  };
}

/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
export const metaReducers: MetaReducer<State>[] = !environment.production
  ? [logger]
  : [];

/**
 * Layout Reducers
 */
export const getLayoutState = createFeatureSelector<fromLayout.State>('layout');

export const getExpandSidebar = createSelector(
  getLayoutState,
  fromLayout.getExpandSidebar
);

/**
 * views Reducers
 */
export const getAllViewState = createFeatureSelector<fromAllview.State>('allview');
export const getByPlatformState = createFeatureSelector<fromByPlatform.State>('byPlatform');


export const getAllState = createFeatureSelector<fromAllview.State>('allview');

export const getAllSate2 = createSelector(
  getAllState,
  fromAllview.getSate
);


/**
 * Monitored Object Reducers
 */
export const getMonitoredObjectsState = createFeatureSelector<fromMonitoredObjects.State>('monitoredObjects');

export const getMonitoredObjects = createSelector(
  getMonitoredObjectsState,
  fromMonitoredObjects.getSate
);

export const getFilteredMonitoredObject = createSelector(
  getMonitoredObjectsState,
  fromMonitoredObjects.filteredMonitoredObject
);
