import * as byPlatform from '../actions/byPlatform';
import {SidebarPageAvailable} from '../components/sidebar/sidebar.component';

export interface State {
  isSidebarOpen: boolean;
  isSearchOpen: boolean;
  sidebarActivePage: SidebarPageAvailable;
  detailMode: boolean;
}

export const initialState: State = {
  isSidebarOpen: false,
  isSearchOpen: true,
  sidebarActivePage: SidebarPageAvailable.Filter,
  detailMode: false
};



export function reducer(state = initialState, action: byPlatform.Actions): State {
  switch (action.type) {
    case byPlatform.BYPLATFORM_TOGGLE_SIDEBAR:
    {
      const newState = state;
      newState.isSidebarOpen = !newState.isSidebarOpen;
      return newState;
    }
    case byPlatform.BYPLATFORM_LOAD_STATE:
      return action.payload.pageState;

    default:
      return state;
  }
}

export const getSate = (state: State) => state;
