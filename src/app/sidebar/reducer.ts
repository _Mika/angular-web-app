import { ActionReducer, Action } from '@ngrx/store';
import { SIDEBARTOGGLE } from './action'

export function sidebarOpenCloseReducer(state: boolean = true, action: Action){
    switch(action.type){
        case SIDEBARTOGGLE:
            return !state;
        default:
            return state;
    }
}