import * as layout from '../actions/layout';

export interface State {
  expandSidebar: boolean;
}

const initialState: State = {
  expandSidebar: true
}

export function reducer(state = initialState, action: layout.Actions): State {
  switch (action.type) {
    case layout.TOGGLE_SIDEBAR:
      return {
        expandSidebar: !state.expandSidebar,
      };

    default:
      return state;
  }
}

export const getExpandSidebar = (state: State) => state.expandSidebar;
