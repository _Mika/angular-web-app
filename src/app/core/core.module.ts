import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AppComponent } from './containers/layout';
import { NotFoundPageComponent } from './containers/not-found-page';
import { SidebarComponent } from './components/sidebar/sidebar';
import { TopnavComponent } from './components/topnav/topnav';

import { StoreModule } from '@ngrx/store';
import {CounterComponent} from "./components/counter/counter";


export const COMPONENTS = [
    AppComponent,
    NotFoundPageComponent,
    SidebarComponent,
    TopnavComponent,
    CounterComponent
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule
      ]
  ,
    declarations: COMPONENTS,
    exports: COMPONENTS,
})

export class CoreModule {
    static forRoot() {
      return {
        ngModule: CoreModule,
        providers: [],
      };
    }
  }
