import { Observable } from 'rxjs/Observable';
import { Component, ChangeDetectionStrategy, OnInit  } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as layoutAction from '../actions/layout';
import config from '../../config';

import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/first';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
    selector: 'layout',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <div class="layout" [style.margin-left]="(expandSidebar$ | async) ? sidebarSizeOpen : sidebarSizeClose">
          <sidebar [sidebarOpen]="expandSidebar$ | async"></sidebar>
          <topnav (activate)="toggleSidebar()"></topnav>
          <!--test sidebar {{param}}-->
          <div class="page-layout" [style.left]="(expandSidebar$ | async) ? sidebarSizeOpen : sidebarSizeClose">
            <router-outlet></router-outlet>
          </div>
        </div>
    `,
    styles: [`
        .layout {
            transition: 0.5s;
            margin-top: 50px;
            overflow: hidden;
        }
        .page-layout {
          position: fixed;
          top: 50px;
          bottom: 0;
          right: 0;
          transition: 0.5s;
        }
    `]
  })
  export class AppComponent implements OnInit {
    expandSidebar$: Observable<boolean>;
    sidebarSizeOpen = config.sidebar.sizeOpen;
    sidebarSizeClose = config.sidebar.sizeClose;

  param: string;

    constructor(private store: Store<fromRoot.State>, private route: ActivatedRoute,
                private router: Router) {

      // this.router.events.subscribe(val => {
      //   console.log(val);//this will give you required url
      // });

        /*get sidebar status*/
        this.expandSidebar$ = this.store.select(fromRoot.getExpandSidebar);
    }

  ngOnInit() {

      console.log(this.route.snapshot.params);
    console.log(this.route.snapshot.paramMap);
    this.param = this.route.snapshot.paramMap.get('id2');
    console.log("-----");
    console.log(this.route.children);
  }
    toggleSidebar() {
      this.store.dispatch(new layoutAction.ToggleSidebar());
    }
}
