import { Action } from '@ngrx/store';

export const TOGGLE_SIDEBAR = '[Layout] Toggle Sidebar';

export class ToggleSidebar implements Action {
  readonly type = TOGGLE_SIDEBAR;
}

export type Actions = ToggleSidebar;