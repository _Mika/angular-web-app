
import { Component, Input, Output, EventEmitter } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Store} from "@ngrx/store";
import * as fromMonitoredObjects from '../../../reducers/monitoredObjects';
import * as fromRoot from '../../../reducers';

@Component({
    selector: 'topnav',
    templateUrl: './topnav.html',
    styleUrls: ['./topnav.css'],
  })
  export class TopnavComponent {
    @Output() activate = new EventEmitter();

  MonitoredObjectsSate$: Observable<fromMonitoredObjects.State>;

  constructor(private store: Store<fromRoot.State>) {
    this.MonitoredObjectsSate$ = this.store.select(fromRoot.getMonitoredObjects);
  }
}
