
import { Component, Input, Output, EventEmitter } from '@angular/core';
import config from '../../../config'

@Component({
    selector: 'sidebar',
    templateUrl: './sidebar.html',
    styleUrls: ['./sidebar.css']
  })
  export class SidebarComponent {
    @Input() sidebarOpen;

    sizeOpen = config.sidebar.sizeOpen;
    sizeClose = config.sidebar.sizeClose;
}
