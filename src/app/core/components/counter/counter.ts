
import { Component, Input} from '@angular/core';


@Component({
  selector: 'app-counter',
  templateUrl: './counter.html',
  styleUrls: ['./counter.css'],
})
export class CounterComponent {
  @Input() count;
  @Input() color;
  @Input() icon;
}
