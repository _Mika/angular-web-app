import { Action } from '@ngrx/store';
import { MonitoredObject } from '../models/monitoredObject.model';

export const RETRIEVE_MONITORED_OBJECTS = '[Monitored Object] Retrieving';
export const UPDATE_MONITORED_OBJECTS = '[Monitored Object] Updating';
export const DELETE_MONITORED_OBJECTS = '[Monitored Object] Removing';
export const SEARCH_MONITORED_OBJECTS = '[Monitored Object] load search';
export const SORT_MONITORED_OBJECTS = '[Monitored Object] Set sort';

export class RetrieveAllMonitoredObjects implements Action {
  readonly type = RETRIEVE_MONITORED_OBJECTS;

  constructor(public payload: { monitoredObjects: MonitoredObject[]}) {}
}

export class UpdateAllMonitoredObjects implements Action {
  readonly type = UPDATE_MONITORED_OBJECTS;

  constructor(public payload: { monitoredObject: MonitoredObject}) {}
}

export class DeleteAllMonitoredObjects implements Action {
  readonly type = DELETE_MONITORED_OBJECTS;

  constructor(public payload: { monitoredObject: MonitoredObject}) {}
}

export class LoadSortMonitoredObjects implements Action {
  readonly type = SORT_MONITORED_OBJECTS;

  constructor(public payload: { currentSort: string}) {}
}

export class LoadSearchMonitoredObjects implements Action {
  readonly type = SEARCH_MONITORED_OBJECTS;

  constructor(public payload: { searchOnMonitoredObjects: string}) {}
}



export type Actions = RetrieveAllMonitoredObjects | UpdateAllMonitoredObjects | DeleteAllMonitoredObjects| LoadSearchMonitoredObjects | LoadSortMonitoredObjects ;
