import { Action } from '@ngrx/store';
import {State} from '../reducers/allview';

export const ALLVIEW_TOGGLE_SIDEBAR = '[All View] Toggle Sidebar';
export const ALLVIEW_SIDEBAR_SELECT_PAGE = '[All View] Switch Sidebar Page';
export const ALLVIEW_LOAD_STATE = '[All View] Load state';

export class AllviewToggleSidebar implements Action {
  readonly type = ALLVIEW_TOGGLE_SIDEBAR;
}

export class AllviewSidebarSelectPage implements Action {
  readonly type = ALLVIEW_SIDEBAR_SELECT_PAGE;
}

export class AllviewLoadState implements Action {
  readonly type = ALLVIEW_LOAD_STATE;

  constructor(public payload: { pageState: State}) {}
}

export type Actions = AllviewToggleSidebar | AllviewSidebarSelectPage | AllviewLoadState ;
