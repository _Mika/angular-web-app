import { Action } from '@ngrx/store';
import {State} from '../reducers/allview';

export const BYPLATFORM_TOGGLE_SIDEBAR = '[By platform View] Toggle Sidebar';
export const BYPLATFORM_SIDEBAR_SELECT_PAGE = '[By platform View] Switch Sidebar Page';
export const BYPLATFORM_LOAD_STATE = '[By platform View] Load state';

export class ByPlatformToggleSidebar implements Action {
  readonly type = BYPLATFORM_TOGGLE_SIDEBAR;
}

export class ByPlatformSidebarSelectPage implements Action {
  readonly type = BYPLATFORM_SIDEBAR_SELECT_PAGE;
}

export class ByPlatformLoadState implements Action {
  readonly type = BYPLATFORM_LOAD_STATE;

  constructor(public payload: { pageState: State}) {}
}

export type Actions = ByPlatformToggleSidebar | ByPlatformSidebarSelectPage | ByPlatformLoadState ;
