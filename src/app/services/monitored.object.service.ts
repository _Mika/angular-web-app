import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {MonitoredObject} from '../models/monitoredObject.model';

import * as io from 'socket.io-client';
import {Store} from '@ngrx/store';
import * as fromRoot from '../reducers';
import * as action from '../actions/monitoredObjects';

@Injectable()
export class MonitoredObjectService {
  private url = 'http://vmmicka:3000';
  private socket;

  constructor(private http: Http, private store: Store<fromRoot.State>) {
    this.socket = io(this.url);
  }

  getMonitoredObject(): Observable<MonitoredObject[]> {
    return this.http.get('http://vmmicka:3000/api/v1/monitored-objects')
      .map(r => {console.log(r.json()); return r.json(); });
  }

  startSocketListenner() {
    this.socket.on('monitored-object.save', resp => this.store.dispatch(
      {type: action.UPDATE_MONITORED_OBJECTS, payload: {monitoredObject: resp}}
      )
    );
    this.socket.on('monitored-object.delete', resp => {
        this.store.dispatch(
          {type: action.DELETE_MONITORED_OBJECTS, payload: {monitoredObject: resp}}
        );
        console.log('socket io delete');
      }
    );
  }

  endSocketListenner() {
    this.socket.disconnect();
  }
}
