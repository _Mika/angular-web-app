import { Routes } from '@angular/router';
import { NotFoundPageComponent } from './core/containers/not-found-page';
import {AllviewComponent} from './containers/allview/allview.component';
import {ByPlatformComponent} from './containers/byplatform/by.platform.component';

export const routes: Routes = [
  { path: '',  pathMatch: 'full', redirectTo: 'allview' },
  // {
  //   path: 'allview',
  //   loadChildren : './views/views.module#ViewsModule'
  // },
  { path: 'allview', component: AllviewComponent },
  { path: 'byplatform', component: ByPlatformComponent },
  { path: '**', component: NotFoundPageComponent }
];
