/**
 * Monitor Object Model
 */
export interface MonitoredObject {
  id: string;
  alias: string;
  enabled: boolean;
  localID: string;
  objectType: ObjectType;
  connectorInformation: ConnectorInformation;
  status: Satus;
  monitoringSource: MonitoringSource;
  parents: any[];
  properties: Properties[];
}


export interface ObjectType {
  id: string;
  version: string;
  composite: boolean;
}
export interface ConnectorInformation {
  id: string;
  version: string;
}

export interface Satus {
  scanDate: string;
  services: Service[];
}

export interface Service {
  icon: string;
  name: string;
  nodes: Node[];
  status: string;
  enabled: boolean;
  severity: number;
}

export interface Node {
  name: string;
  error: string;
  nodes: Node[];
  status: string;
  enabled: boolean;
  severity: number;
  childrenErrors?: any[];
}


export interface MonitoringSource {
  id: string;
  name: string;
  port: number;
  state: string;
  hostname: string;
  stateDate: string;
  errorMessage: string;
}


export interface Properties {
  propertyValueId: string;
  propertyKeyId: string;
}
