import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

export enum SidebarPageAvailable {
  Filter = 0,
  Sort = 1
}

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public sidebarPageAvailable = SidebarPageAvailable;

  @Output() toggleSidebar = new EventEmitter();
  @Output() sidebarPageSelection = new EventEmitter();
  @Output() sidebarSortSelection = new EventEmitter();
  @Input() sidebarOpen;
  @Input() sidebarActivePage;

  sideBarWightOpen = '200px';
  sideBarWightClose = '0px';
  // page = 'filter';

  constructor() { }

  ngOnInit() {
    console.log(SidebarPageAvailable['Filter']);
  }
  // selectPage(page) {
  //   this.page = page;
  // }
}
