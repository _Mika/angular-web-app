import {Component, Input, OnInit} from '@angular/core';
import {MonitoredObject} from '../../models/monitoredObject.model';

@Component({
  selector: 'app-rows-allview',
  templateUrl: './rows.allview.component.html',
  styleUrls: ['./rows.allview.component.css']
})
export class RowsAllviewComponent implements OnInit {
  @Input() monitoredObjects: MonitoredObject[];
  @Input() detail: boolean;

  constructor() { }

  ngOnInit() {
  }

}
