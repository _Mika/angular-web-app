import {Component, Input, OnInit} from '@angular/core';
import {MonitoredObject} from '../../models/monitoredObject.model';

@Component({
  selector: 'app-row',
  templateUrl: './row.by.platform.component.html',
  styleUrls: ['./row.by.platform.component.css']
})
export class RowComponent implements OnInit {

  @Input() monitoredObject: MonitoredObject;

  @Input() detail: boolean;

  constructor() { }

  ngOnInit() {
  }

}
