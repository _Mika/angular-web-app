import {Component, Input, OnInit} from '@angular/core';
import {MonitoredObject} from '../../models/monitoredObject.model';

@Component({
  selector: 'app-rows-by-platform',
  templateUrl: './rows.by.platform.component.html',
  styleUrls: ['./rows.by.platform.component.css']
})
export class RowsComponent implements OnInit {
  @Input() monitoredObjects: MonitoredObject[];
  @Input() detail: boolean;

  constructor() { }

  ngOnInit() {
  }

}
