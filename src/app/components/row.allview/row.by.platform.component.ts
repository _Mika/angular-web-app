import {Component, Input, OnInit} from '@angular/core';
import {MonitoredObject} from '../../models/monitoredObject.model';

@Component({
  selector: 'app-row-allview',
  templateUrl: './row.allview.component.html',
  styleUrls: ['./row.allview.component.css']
})
export class RowByPlatformComponent implements OnInit {

  @Input() monitoredObject: MonitoredObject;

  @Input() detail: boolean;

  constructor() { }

  ngOnInit() {
  }

}
