import {RouterAction, ROUTER_NAVIGATION, RouterNavigationAction} from '@ngrx/router-store';
import {Actions, Effect} from '@ngrx/effects';
import {Params, ActivatedRouteSnapshot} from '@angular/router';
import {Store, combineReducers} from '@ngrx/store';
import {Injectable} from '@angular/core';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';

import {MonitoredObjectService} from '../services/monitored.object.service';
import {initialState, State} from '../reducers/allview';

import * as allview from '../actions/allview';
import * as byPlatform from '../actions/byPlatform';
import * as monitoredObjects from '../actions/monitoredObjects';
import * as fromMonitoredObjects from '../reducers/monitoredObjects';
import * as fromRoot from '../reducers';

import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class MOEffects {
  MonitoredObjectsSate$: Observable<fromMonitoredObjects.State>;
  constructor(private actions: Actions, private store: Store<State>, private mOService: MonitoredObjectService) {
    this.MonitoredObjectsSate$ = this.store.select(fromRoot.getMonitoredObjects);
  }

  @Effect() navigateToAllview = this.handleNavigation('allview', (r: ActivatedRouteSnapshot) => {

    let state: State;
    try {

      state = createParams(r.params);
    } catch (e) {
      console.log('Fail to load parametter, loading initial state');
      state = initialState;
    }

    // Load State
    this.store.dispatch({type: allview.ALLVIEW_LOAD_STATE, payload: {pageState: state}});
    // Load Search
    this.store.dispatch(
      {type: monitoredObjects.SEARCH_MONITORED_OBJECTS,
        payload: {searchOnMonitoredObjects: state.searchOnMonitoredObjects}});

    // Load Sort
    this.store.dispatch(
      {type: monitoredObjects.SORT_MONITORED_OBJECTS,
        payload: {currentSort: state.currentSort}});

    const result = this.MonitoredObjectsSate$.map(
      (state) => {
        console.log('yolololololol 32', state);
        if (!state.loaded) {
          console.log('oesosefjesff 33');
          return this.mOService.getMonitoredObject().subscribe(resp => {
            console.log('oesosefjesff 34', resp);
            this.store.dispatch({type: monitoredObjects.RETRIEVE_MONITORED_OBJECTS, payload: {monitoredObjects: resp}});
          });
        } else {
          return of({});
        }
      }
    );
    result.subscribe(result => result);

    return of();
  });

  @Effect() navigateToByPlatform = this.handleNavigation('byplatform', (r: ActivatedRouteSnapshot) => {

    let state: State;
    try {
      state = createParams(r.params);
    } catch (e) {
      console.log('Fail to load parametter, loading initial state');
      state = initialState;
    }
    // Load State
    this.store.dispatch({type: byPlatform.BYPLATFORM_LOAD_STATE, payload: {pageState: state}});
    const result = this.MonitoredObjectsSate$.map(
      (state) => {
        if (!state.loaded) {
          return this.mOService.getMonitoredObject().subscribe(resp => {
            this.store.dispatch({type: monitoredObjects.RETRIEVE_MONITORED_OBJECTS, payload: {monitoredObjects: resp}});
          });
        } else {
          return of({});
        }
      }
    );
    result.subscribe(result => result);
    return of();
  });


  private handleNavigation(segment: string, callback: (a: ActivatedRouteSnapshot, state: State) => Observable<any>) {
    const nav = this.actions.ofType(ROUTER_NAVIGATION).
    map(firstSegment).
    filter(s => s.routeConfig.path === segment);

    return nav.withLatestFrom(this.store).switchMap(a => callback(a[0], a[1])).catch(e => {
      console.log('Network error', e);
      return of();
    });
  }
}

function firstSegment(r: RouterNavigationAction) {
  return r.payload.routerState.root.firstChild;
}

function createParams(stateToLoad: Params): State {
  // return {speaker: p['speaker'] || null, title: p['title'] || null, minRating: p['minRating'] ? +p['minRating'] : 0};

  const sate: State = initialState;
  for (let key in sate) {
    if (stateToLoad[key]) {
      if (typeof sate[key] === 'boolean' && typeof stateToLoad[key] === 'string') {
        sate[key] = stateToLoad[key] === 'true' ? true : false;
      } else if (typeof sate[key] === 'number') {
        sate[key] = Number(stateToLoad[key]);
      } else {
        sate[key] = stateToLoad[key];
      }
    }else {
      if (typeof stateToLoad[key] === 'string') {
        sate[key] = '';
      } else {
        sate[key] = initialState[key];
      }
    }

    // Use `key` and `value`
  }
  return sate;
}
