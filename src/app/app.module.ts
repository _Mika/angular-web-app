import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './core/containers/layout';

import { EffectsModule } from '@ngrx/effects';

// reducer
import { StoreModule } from '@ngrx/store';
import { sidebarOpenCloseReducer } from './sidebar/reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers, metaReducers } from './reducers';

import { CoreModule } from './core/core.module';
import { routes } from './routes';
import {StoreRouterConnectingModule} from "@ngrx/router-store";
import { environment } from '../environments/environment';
import {MOEffects} from "./effects/views.effects";
import {MonitoredObjectService} from "./services/monitored.object.service";
import {HttpModule} from "@angular/http";
import {AllviewComponent} from "./containers/allview/allview.component";
import {SidebarComponent} from "./components/sidebar/sidebar.component";
import { RowComponent } from './components/row.by.platform/row.by.platform.component';
import { RowsComponent } from './components/rows.by.platform/rows.by.platform.component';
import {ByPlatformComponent} from "./containers/byplatform/by.platform.component";
import {RowsAllviewComponent} from "./components/rows.allview/rows.allview.component";
import {RowByPlatformComponent} from "./components/row.allview/row.by.platform.component";


@NgModule({
  declarations: [
    AllviewComponent,
    ByPlatformComponent,
    SidebarComponent,
    RowComponent,
    RowByPlatformComponent,
    RowsComponent,
    RowsAllviewComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, { useHash: true }),
    StoreModule.forRoot(reducers, { metaReducers }),

    CoreModule.forRoot(),
    /**
     * @ngrx/router-store keeps router state up-to-date in the store.
     */
    StoreRouterConnectingModule,

    /**
     * Store devtools instrument the store retaining past versions of state
     * and recalculating new states. This enables powerful time-travel
     * debugging.
     *
     * To use the debugger, install the Redux Devtools extension for either
     * Chrome or Firefox
     *
     * See: https://github.com/zalmoxisus/redux-devtools-extension
     */
    !environment.production ? StoreDevtoolsModule.instrument() : [],

    /**
     * EffectsModule.forRoot() is imported once in the root module and
     * sets up the effects class to be initialized immediately when the
     * application starts.
     *
     * See: https://github.com/ngrx/platform/blob/master/docs/effects/api.md#forroot
     */
    EffectsModule.forRoot([MOEffects]),

    StoreDevtoolsModule.instrument({
      maxAge: 5
    }),
    HttpModule
  ],
  providers: [
    MOEffects,
  MonitoredObjectService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
